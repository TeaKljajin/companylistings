@extends('layouts.app')
@section('content')
    <div class=”container” style="padding-left:10%;padding-right: 10%;">

        <div class="panel panel-default">
            <a href="{{route('dashboard')}}" class="btn btn-primary" style="margin-right:70%;" role="button">Go Back</a>
            <div class="panel-body">
                <h1 class="text-center">Edit Listing</h1>


                {!! Form::model($listing,['method'=>'POST','action'=>['ListingsController@update',$listing->id]]) !!}
                {!! csrf_field() !!}
                <div class="row text-center">

                    <div class="col-6">
                        <div class="form-group">
                            {!! Form::label('name','Name:') !!}
                            {!! Form::text('name', null, ['class'=>'form-control']) !!}


                        </div>
                        <div class="form-group">
                            {!! Form::label('address','Address:') !!}
                            {!! Form::text('address', null, ['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('website','Website:') !!}
                            {!! Form::text('website', null, ['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('email','Email:') !!}
                            {!! Form::email('email', null, ['class'=>'form-control']) !!}
                        </div>
                    </div>
                    <div class="col-6">

                        <div class="form-group">
                            {!! Form::label('phone','Phone:') !!}
                            {!! Form::number('phone', null, ['class'=>'form-control']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('bio','Bio:') !!}
                            {!! Form::textarea('bio', null, ['class'=>'form-control','rows'=>8]) !!}
                        </div>

                    </div>
                    <div class="form-group" style="margin-left:92%;">
                        {!! Form::submit('Update',['class'=>'btn btn-success']) !!}

                    </div>

                </div>
                {!! Form::close() !!}
                @if(count($errors) > 0)
                    @foreach($errors->all() as $error)
                        <div class="btn btn-danger col-sm-offset-2 mt-3">
                            {{$error}};
                        </div>

                    @endforeach
                @endif


            </div>
        </div>

    </div>

@endsection


