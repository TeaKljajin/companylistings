@extends('layouts.app')
@section('content')
    <div class=”container” style="padding-left:10%;padding-right:10%">
        <div class="row text-center">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    @if(session('success'))
                        <div class="alert alert-success text-left">
                            {{session('success')}}
                        </div>
                    @endif
                    <a href="{{route('createListing')}}" class="btn btn-primary" style="margin-right:85%;" role="button">Create New Listing</a>
                    <div class="panel-body">
                        <h1>All listings by {{Auth::user()->name}}</h1>
                        @if(count($user->listings)>0)
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                         <th>Company Name</th>
                                         <th>Address</th>
                                        <th>Website</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Bio</th>
                                        <th>Edit</th>
                                        <th>Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($user->listings as $listing)
                                     <tr>
                                        <td>{{$listing->name}}</td>
                                         <td>{{$listing->address}}</td>
                                         <td>{{$listing->website}}</td>
                                         <td>{{$listing->email}}</td>
                                         <td>{{$listing->phone}}</td>
                                         <td>{{$listing->bio}}</td>
                                         <td><a href="{{route('editListing', $listing->id)}}">Edit</a></td>
                                         <td>
                                             {!! Form::open(['method'=>'POST','action'=>['ListingsController@destroy',$listing->id]]) !!}
                                             {!! csrf_field() !!}

                                             <button id="btnDelete"class="btn btn-danger btn-sm" onclick="return confirm('Are you sure that you wont to  delete?')">Delete</button>
                                             {!! Form::close() !!}
                                         </td>
                                     </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
