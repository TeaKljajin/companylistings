

    @extends('layouts.app')
@section('content')
    <div class=”container” style="padding-left:10%;padding-right:10%">
        <div class="row text-center">
            <div class="col-md-12 ">
                <div class="panel panel-default">
                    <div class="col-lg-12">
                        <p>This app is made that every person can register his/her own company. Person can create as much as he won't companies.</p>
                        <p>When person is logged on this site, person will see only his companies. Person can edit, delete company witch are made by him/her.</p>
                    </div>
                    <div class="panel-body">
                        <h1>Listings by all Companies</h1>

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Company Name</th>
                                    <th>Address</th>
                                    <th>Website</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Bio</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($listings as $listing)
                                    <tr>
                                        <td>{{$listing->name}}</td>
                                        <td>{{$listing->address}}</td>
                                        <td>{{$listing->website}}</td>
                                        <td>{{$listing->email}}</td>
                                        <td>{{$listing->phone}}</td>
                                        <td>{{$listing->bio}}</td>


                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


