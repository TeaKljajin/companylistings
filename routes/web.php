<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PublicController@index')->name('main');


Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('listings/create', 'ListingsController@create')->name('createListing');
Route::post('listings/create', 'ListingsController@store')->name('storeListing');
Route::get('listings/edit/{id}', 'ListingsController@edit')->name('editListing');
Route::post('listings/update/{id}', 'ListingsController@update')->name('updateListing');
Route::post('listings/delete/{id}', 'ListingsController@destroy')->name('destroyListing');
