<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;

class PublicController extends Controller
{
    public function index(){
        $listings = Listing::all();
        return view('main', compact('listings'));
    }

}
